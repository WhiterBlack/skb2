var express = require('express');
const url = require('url');
var cors = require('cors');
var bodyParser = require('body-parser');
const app = express();
app.use(cors());
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: true ,limit: '5mb'}));


app.use('/task3b',require('./task3b'));
app.get('/calculator', (req, res) => {
  /*
    Складывание чисел.
    Прошел все 5 тестов
    Код краткий но работает как надо.
    Почему то express отка
   */
  res.send(((Number(req.query.a) || 0)+(Number(req.query.b) || 0)).toString());
});

app.get('/fio',(req,res) => {
  var input = req.query.fullname;
  const INVALID_DATA_MSG = 'Invalid fullname';
  //Проверка на пустоту
  if(!input) return res.send(INVALID_DATA_MSG);
  //Проверка на ошибки
  if(new RegExp('[0-9]|_|\/','g').test(input)) return res.send(INVALID_DATA_MSG);
  input = input.split(' ');
  //Удаление пустых элементов массива
  let _input = [];
  for(let i in input) (input[i] !='')?_input.push(input[i]):null;
  input = _input;
  //Сам парсинг
  let output = '';
  switch(input.length) {
    case 1: {
      output = FirstCharToUpper(input[0].toLowerCase());
      break;
    }
    case 2: {
      output = `${FirstCharToUpper(input[1].toLowerCase())} ${input[0][0].toUpperCase()}.`;
      break;
    }
    case 3: {
      output = `${FirstCharToUpper(input[2].toLowerCase())} ${input[0][0].toUpperCase()}. ${input[1][0].toUpperCase()}.`;
      break;
    }
    default: {
      output = INVALID_DATA_MSG;
    }
  }
  res.send(output);
});

app.get('/nickname', (req,res) => {
  //Можно было бы уложится и в одни regex, но я с ними пока не очень хорошо дружу...
  var input = req.query.username;
  if(!input) return res.send('Invalid username');
  input = input.replace(new RegExp('http[s]?'),'');//Удаляем http https
  input = input.replace(new RegExp(':?[\/\/]*[a-z.0-9-]*\/'),'');//Удаляем домен
  input = input.split('/')[0];//Получем первое вхождение после слеша
  res.send(200,input[0] !='@'? `@${input}`:input);//Отслылаем, если надо добавляем собаку
});

var _color = require('parse-color');
var RGBColor = require('rgbcolor');
app.get('/color',function (req,res) {
  var c = req.query.color;
  if(!c) {
    return res.send(200,'Invalid color');
  }
  c = c.replace(new RegExp(' ','g'),'');
  if(c[0] != '#' & c.indexOf('rgb') === -1 & c.indexOf('hsl') === -1) {
    c = `#${c}`;
  }
  if(new RegExp('hsl').test(c)) {
    return res.send(200,hsl(c));
  }
  var color = new RGBColor(c);
  if(color.ok) {
    //
    if(new RegExp('rgb').test(c)) {
      let match =c.match(new RegExp('([0-9]*)','g'));
      if(Number(match[4]) >= 256 || Number(match[6]) >= 256 || Number(match[8]) >= 256) {
        return res.send(200,'Invalid color');
      }
    }
    //
    if(c.length > 7  & c.indexOf('rgb') === -1 & c.indexOf('hsl') === -1) {
      return res.send(200,'Invalid color');
    }
    if(_color(c).hex) {
      res.send(200,color.toHex());
    } else {
      res.send(200,'Invalid color');
    }
  } else {
    res.send(200,'Invalid color');
  }
});
var _ = require('lodash');
function hsl(col) {
  let match = col.replace(new RegExp('%20','g'),'').match(new RegExp('(-?[0-9]*%)','g'));
  for(let i in match) {
    if(parseInt(match[i]) > 100 || parseInt(match[i]) < 0) {
      return 'Invalid color';
    }
  }
  //
  match = _.compact(col.replace(new RegExp('%20','g'),'').match(new RegExp('[0-9]*%?','g')));
  if(match[1][match[1].length-1] === '%' && match[2][match[2].length-1] === '%') {
    var c = _color(col.replace(new RegExp('%20','g'),''));
    return c.hex || 'Invalid color';
  }

  return 'Invalid color';
}
/*
var color = require('parse-color');
app.get('/color',function (req,res) {
  var c = req.query.color;
  c = c.replace(new RegExp(' '),'');
  if(c[0] != '#') {
    c = `#${c}`;
  }
  if(c.length < 4) {
    return res.send(200,'Invalid color');
  }
  res.send(200,color(c).hex || 'Invalid color');
});
*/

app.use('/3',require('./skb3/skb3'));
function FirstCharToUpper(str) {
  if (!str) return str;
  return str[0].toUpperCase() + str.slice(1);
}

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});