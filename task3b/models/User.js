/**
 * Created by whiterblack on 24.11.16.
 */
var mongoose = require('../lib/mongoose');
var Schema = mongoose.Schema;
var schema = new Schema({
    id:Number,
    username:String,
    password:String,
    values:{
        money:String,
        origin:String
    }
});
schema.method('toJSON', function() {
    var user = this.toObject();
    delete user._id;
    delete user.__v;
    return user;
});
exports.User = mongoose.model('User',schema);