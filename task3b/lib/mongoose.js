/**
 * Created by WhiterBlack on 29.07.2016.
 */
var mongoose = require('mongoose');

var log = require('./logger')(module);

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1/task3b');
log.info(`Mongoose version: ${mongoose.version}`);
var db = mongoose.connection;

var ConAttempts = 0;
db.once('open', function() {
    ConAttempts = 0;
    var admin = new mongoose.mongo.Admin(mongoose.connection.db);
    admin.buildInfo(function (err, info) {
        log.info(`Mongodb connected: Version: ${info.version}`);
    });
});


db.on('error', function (err) {
    log.error('connection error:', err.message);
    ConAttempts++;
    if(config.get('database:reconnect:enabled')) {
        setTimeout(function Reconnect() {
            if(ConAttempts<config.get('database:reconnect:Attempts') || config.get('database:reconnect:Attempts')===0) {
                mongoose.connect(config.get('database:url',config.get('database:params')));
            } else {
                ErrorAndClose(err);
            }
        },config.get('db:reconnect:interval'))
    } else {
        ErrorAndClose(err)
    }
});

function ErrorAndClose(err) {
    log.error(`connection error: ${err.message}. Attempts: ${ConAttempts}`);
    process.exit(-1);
}
module.exports = mongoose;