/**
 * Created by WhiterBlack on 26.06.2016.
 */
var winston = require('winston');
function getLogger(module) {
    var path = module.filename.split(require('path').sep).slice(-2).join('/');
    //Вывод в консоли
    var transports = [
        new winston.transports.Console({
            timestamp:function() {return new Date().toLocaleString(); },
            colorize:true,
            label: path,
            //level:'debug'
        })
    ];
    return new winston.Logger({
        transports: transports
    });
}
module.exports = getLogger;