/**
 * Created by whiterblack on 24.11.16.
 */
var express = require('express');
var router = express.Router();
var _ = require('lodash');
var async = require('async');
var request =require('request');

var User = require('./models/User').User;
var Pet = require('./models/Pet').Pet;

router.use(function (req,res,next) {
    console.log(req.originalUrl);
    next();
})
var pets = JSON.parse(require('fs').readFileSync(__dirname+'/pets.json'));
request('https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json',function (err,req,body) {
    err?console.error(err):console.log('Новые данные загружены...');
    pets = JSON.parse(body);
});
/*

router.get('/',function (req,res) {
    async.parallel({
        users: function (cb) {
            Pet.find({},cb)
        },
        pets: function (cb) {
            User.find({},cb)
        }
    },function (err,data) {
        data.pets = _.map(data.pets,(function (pet) {
            delete pet._id;
            return pet;
        }));
        data.users = _.map(data.users,(function (pet) {
            delete pet._id;
            return pet;
        }));
        res.json(data);
    })
})
*/
router.get('/',function (req,res) {
    res.json(pets);
})
router.get('/users',function (req,res) {
    if(req.query.havePet) {
        var retdata = [];
        for(var i in pets.users) {
            for(var q in pets.pets) {
                if(pets.pets[q].userId == pets.users[i].id) {
                    if(pets.pets[q].type == req.query.havePet) {
                        retdata.push(pets.users[i]);
                        break;
                    }

                }
            }
        }
        res.json(retdata);
    } else {
        res.json(pets.users);
    }
});

router.get('/users/populate',function (req,res) {
    var _users = pets.users;
    if(req.query.havePet) {
        var retdata = [];
        for(var i in _users) {
            for(var q in pets.pets) {
                if(pets.pets[q].userId == _users[i].id) {
                    if(pets.pets[q].type == req.query.havePet) {
                        retdata.push(_users[i]);
                        break;
                    }

                }
            }
        }
        //

        for(var i in retdata) {
            retdata[i].pets = [];
            for(var q in pets.pets) {
                if(pets.pets[q].userId == retdata[i].id) {
                    retdata[i].pets.push(pets.pets[q]);
                }
            }
        }
        //
        res.json(retdata);
    } else {
        for(var i in _users) {
            _users[i].pets = [];
            for(var q in pets.pets) {
                if(pets.pets[q].userId == _users[i].id) {
                    delete pets.pets[q].user;
                    _users[i].pets.push(pets.pets[q]);
                }
            }
        }
        console.log(_users)
        res.json(_users);
    }
});
router.get('/users/:id/populate',function (req,res) {
    for(var i in pets.users) {
        if(pets.users[i].id == req.params.id) {
            var _users = pets.users[i];
            _users.pets = [];
            for(var q in pets.pets) {
                if(pets.pets[q].userId == _users.id) {
                    delete pets.pets[q].user;
                    _users.pets.push(pets.pets[q]);
                }
            }
            return res.json(_users);
        }
    }
    req.next();
});


router.get('/users/:id',function (req,res) {
    for(var i in pets.users) {
        if(pets.users[i].id == req.params.id) {
             return res.json(pets.users[i]);
        }
    }
    req.next();
});
router.get('/users/:username',function (req,res) {
    for(var i in pets.users) {
        if(pets.users[i].username == req.params.username) {
            return res.json(pets.users[i]);
        }
    }
    req.next();
});
router.get('/users/:username/populate',function (req,res) {
    for(var i in pets.users) {
        if(pets.users[i].username == req.params.username) {
            var _users = pets.users[i];
            _users.pets = [];
            for(var q in pets.pets) {
                if(pets.pets[q].userId == _users.id) {
                    delete pets.pets[q].user;
                    _users.pets.push(pets.pets[q]);
                }
            }
            return res.json(_users);
        }
    }
    req.next();
});

router.get('/users/:username/pets',function (req,res) {
    for(var i in pets.users) {
        if(pets.users[i].username == req.params.username) {
            var id = pets.users[i].id;
            var _pets = [];
            for(var i in pets.pets) {
                if(pets.pets[i].userId == id) {
                    _pets.push(pets.pets[i]);
                }
            }
            return res.json(_pets);
        }
    }
    req.next();
});
router.get('/users/:id/pets',function (req,res) {
    for(var i in pets.users) {
        if(pets.users[i].id == req.params.id) {
            var id = pets.users[i].id;
            var _pets = [];
            for(var i in pets.pets) {
                if(pets.pets[i].userId == id) {
                    _pets.push(pets.pets[i]);
                }
            }
            return res.json(_pets);
        }
    }
    req.next();
});
router.get('/pets',function (req,res) {
    var _pets = [];
    var __pets = [];
    if(req.query.type) {
        for(var i in pets.pets) {
            if(pets.pets[i].type == req.query.type) {
                _pets.push(pets.pets[i]);
            }
        }

    } else {
        _pets = pets.pets;
    }
    if(req.query.age_gt) {
        for(var i in _pets) {
            if(_pets[i].age > Number(req.query.age_gt)) {
                __pets.push(_pets[i]);
            }
        }
        _pets = __pets;
        __pets = [];
    }
    if(req.query.age_lt) {
        for(var i in _pets) {
            if(_pets[i].age < Number(req.query.age_lt)) {
                __pets.push(_pets[i]);
            }
        }
        _pets = __pets;
    }
    res.json(_pets);
});
router.get('/pets/:id',function (req,res) {
    for(var i in pets.pets) {
        if(pets.pets[i].id == req.params.id) {
            return res.json(pets.pets[i]);
        }
    }
    req.next();
});
router.get('/pets/:id/populate',function (req,res) {
    for(var _i in pets.pets) {
        if(pets.pets[_i].id == req.params.id) {
            var _pets = pets.pets[_i];
            for(var q in pets.users) {
                if(_pets.userId == pets.users[q].id) {
                    _pets.user = pets.users[q];
                    break;
                }
            }
            return res.json(_pets);
        }
    }
    req.next();
});

router.get('/pets/populate',function (req,res) {
    var _pets = [];
    var __pets = [];
    if(req.query.type) {
        for(var i in pets.pets) {
            if(pets.pets[i].type == req.query.type) {
                _pets.push(pets.pets[i]);
            }
        }

    } else {
        _pets = pets.pets;
    }
    if(req.query.age_gt) {
        for(var i in _pets) {
            if(_pets[i].age > Number(req.query.age_gt)) {
                __pets.push(_pets[i]);
            }
        }
        _pets = __pets;
        __pets = [];
    }
    if(req.query.age_lt) {
        for(var i in _pets) {
            if(_pets[i].age < Number(req.query.age_lt)) {
                __pets.push(_pets[i]);
            }
        }
        _pets = __pets;
    }
    for(var i in _pets) {
        for(var q in pets.users) {
            if(_pets[i].userId == pets.users[q].id) {
                _pets[i].user = pets.users[q];
                break;
            }
        }
    }
    res.json(_pets)
})
router.use(function (req,res) {
    res.send(404,'Not Found');
});

module.exports = router;