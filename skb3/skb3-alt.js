/**
 * Created by whiterblack on 14.11.16.
 */

var express = require('express');
var router = express.Router();
var request = require('request');
var _ = require('lodash');
var path =require('path');
var nconf = require('nconf').file({ file: path.join(process.cwd(),'/skb3/pc.json') });

var _data;
request('https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json',{json:true},function (err,req,body) {
    err?console.error(err):console.log('[skb3-alt] Новые данные загружены...');
    _data = body;
    nconf.set(null,body);
    //nconf.defaults(body);
});

router.get('/volumes',function (req,res) {
    var volumes = {};
    var pc = nconf.get();
    for(let i in pc.hdd) {
        volumes[pc.hdd[i].volume] = (volumes[pc.hdd[i].volume] || 0) + pc.hdd[i].size;
    }
    for(let i in volumes) {
        volumes[i] = `${volumes[i]}B`;
    }
    res.json(volumes);
});
router.get('/*',function (req,res,next) {
    //Убираем конечный / если он есть
    var search = req.params[0].replace(new RegExp('\/$','g'),'');
    // заменяем слеши на двоеточия
    search = search.replace(new RegExp('\/','g'),':');
    //Проверка на массив
    var LengthRegex = new RegExp('([a-z:]*):length');
    if(search.match(LengthRegex)) {
        if((_.isArray(nconf.get(search.match(LengthRegex)[1]))) || (_.isString(nconf.get(search.match(LengthRegex)[1])))) {
           return next();
        }
    }
    //Запрашиваем у nconf
    var ret = nconf.get(search || null);
    //Если undefirend то посылаем *** на 404
    if(_.isUndefined(ret)) return next();
    //Отдаём...
    res.send(200,JSON.stringify(ret));
});


router.use(function (err,req,res,next) {
    console.error(err);
    res.send(500,err.toString());
});
router.use(function (req,res) {
    res.send(404,'Not Found');
});

module.exports = router;