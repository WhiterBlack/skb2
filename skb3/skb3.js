/**
 * Created by whiterblack on 11.11.16.
 */
var express = require('express');
var router = express.Router();
var request = require('request');
var _ = require('lodash');

router.use('/alt',require('./skb3-alt'));

var pc = JSON.parse(require('fs').readFileSync(__dirname+'/pc.json'));
request('https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json',function (err,req,body) {
    err?console.error(err):console.log('Новые данные загружены...');
    pc = JSON.parse(body);
});
//Роут для получения инфы о разделах
router.get('/volumes',function (req,res) {
    var volumes = {};
    for(var i in pc.hdd) {
        volumes[pc.hdd[i].volume] = (volumes[pc.hdd[i].volume] || 0) + pc.hdd[i].size;
    }
    for(var i in volumes) {
        volumes[i] = `${volumes[i]}B`;
    }
    res.json(volumes);
});
router.get(new RegExp('[a-z]\/length'),function (req,res) {
    req.next(404);
})
router.get('/*',function (req,res,next) {
    var params =  _.compact(req.params[0].split('/'));
    try {
        var ret = pc;
        for(var i in params) {
            ret = ret[params[i]];
        }
        console.log('---------------------------------------------------');
        console.log(req.params[0]);
        console.log(params);
        console.log(ret);
        console.log(`_.isUndefined: ${_.isUndefined(ret)} _.isNull: ${_.isNull(ret)}`);

        (!_.isUndefined(ret) && !_.isNull(ret)) || _.isNull(ret)?res.json(ret):next();
    }catch (err) {
        next();
    }
});
router.use(function (err,req,res,next) {
    if(err === 404) return next();
    console.error(err);
    res.send(500,err);
})
router.use(function (req,res) {
    res.send(404,'Not Found');
})

module.exports = router;